var express =require('express');
var userFile =require('./user.json');
var requestJSON = require('request-json');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json());

const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;


var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu22db/collections/';
const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';


// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users',
function(req, res) {
  //console.log("GET /apitechu/v1/users");
  var httpClient = requestJSON.createClient(baseMLabURL);
  console.log("En proceso.");
  var queryString = 'f={"_id":0}&';
  httpClient.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "Ningún elemento 'user'."}
          res.status(404);
        }
      }
      res.send(response);
    });
});

// GET users con un ID de mLab
app.get(URL_BASE + 'users/:id',
 function (req, res) {
   console.log(req.params.id);
   var id = req.params.id;
   var queryString = 'q={"userID":' + id + '}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body){
       console.log(body.length);
       var respuesta = body;
       res.send(respuesta);
     });
});

// POST 'users' mLab
app.post(URL_BASE + 'users',
 function(req, res){
   var clienteMlab = requestJSON.createClient(baseMLabURL);
   clienteMlab.get('user?' + apikeyMLab,
     function(error, respuestaMLab, body){
       nuevoID = body.length + 1;
       console.log("NuevoID:" + nuevoID);GET
       var newUser = {
         "userID" : nuevoID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
       };
       clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
         function(error, respuestaMLab, body){
           console.log(body);
           res.status(201);
           res.send(body);
         });
     });
 });

 //PUT users con parámetro 'id'
 app.put(URL_BASE + 'users/:id',
 function(req, res) {
  var id = req.params.id;
  console.log(id);
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = requestJSON.createClient(baseMLabURL);
  clienteMlab.get('user?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(req.body);
     clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body); // se visualiza valor 1 si se ejecuta correctamente
       res.send(body);
      });
    });
 });

 //DELETE user with id
 app.delete(URL_BASE + "users/:id",
  function(req, res){
    var id = req.params.id;
    var queryStringID = 'q={"userID":' + id + '}&';
    console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        //Impresion id;
        console.log(queryStringID);
        var respuesta = body[0];
          httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });


  //METODO POST login
  app.post(URL_BASE + "login",
   function (req, res){
     let email = req.body.email;
     let pass = req.body.password;
     let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
     let limFilter = 'l=1&';
     let clienteMlab = requestJSON.createClient(baseMLabURL);
     clienteMlab.get('user?'+ queryString + limFilter + apikeyMLab,
       function(error, respuestaMLab, body) {
         if(!error) {
           if (body.length == 1) { // Existe un usuario que cumple 'queryString'
             let login = '{"$set":{"logged":true}}';
              console.log("aqui");
             clienteMlab.put('user?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(login),
             //Igual se inserta un campo para MYSQL - consultar??
               function(errPut, resPut, bodyPut) {
                 res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
                 console.log("correcto");
               });
           }
           else {
             res.status(404).send({"msg":"Usuario no válido."});
           }
         } else {
           res.status(500).send({"msg": "Error en petición a BD."});
         }
     });
  });


//Method POST logout
app.post(URL_BASE + "logout",
 function (req, res){
   let email = req.body.email;
   let passwd = req.body.password;
   let queryString = 'q={"email":"' + email + '","password":"' + passwd + '"}&';
   let limFilter = 'l=1&';
   let clienteMlab = requestJSON.createClient(baseMLabURL);
   clienteMlab.get('user?'+ queryString + limFilter + apikeyMLab,
     function(error, respuestaMLab, body) {
       if(!error) {
         if (body.length == 1) { // Existe un usuario que cumple condicion
           //let login = '{"$set":{"logged":true}}';
           let logout = '{"$unset":{"logged":true}}';
            console.log("aqui");
           clienteMlab.put('user?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(logout),
           //Igual se inserta un campo para MYSQL??
             function(errPut, resPut, bodyPut) {
               res.send({'msg':'Logout correcto!', 'user':body[0].email, 'userid':body[0].id});
             });
         }
         else {
           res.status(404).send({"msg":"Usuario no válido."});
         }
       } else {
         res.status(500).send({"msg": "Error en petición a BD."});
       }
   });
});



//funcion que devuelve el usuario con ID mayor.
 function nextUserId(callback) {
 let maxUserId = 1;
 let clienteMlab = requestJSON.createClient(baseMLabURL);
 //let clienteMlab = requestJSON.createClient(baseMLabURL);
 let queryObject = { "userID": -1 };
 let queryStringEmail = 's=' + JSON.stringify(queryObject) + '&l=1&' + apikeyMLab;
 // uusario con maximo ID
 clienteMlab.get('user?' + queryStringEmail, function(error, respuestaM , body) {
   console.log(body);
   if (!error) {
     if (body.length > 0) {
       maxUserId = body[0].userID + 1;
     }
   } else {
     maxUserId = -1;
   }
   callback(maxUserId);
 });
 }

nextUserId(function(a){
  console.log(a);
});


app.listen(PORT,function(){
 console.log('APIMLAB TechU! eschuchando en puerto 3000...')

});
